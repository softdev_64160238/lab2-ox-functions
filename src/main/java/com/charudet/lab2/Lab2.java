/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.charudet.lab2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Lab2 {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private static char currentPlayer = 'x';
    private static int row;
    private static int col;
    private static void switchPlayer() {
        if (currentPlayer == 'x') {
            currentPlayer = 'o';
        }
        else {
            currentPlayer = 'x';
        }
    }
    public static void printNameProgram() {
        System.out.println("Tic-Tac-Toe!");
    }
    
    public static void printText() {
        System.out.println("Current board layout:");
    }
    
    public static void printTurn() {
        System.out.println("Player"+" "+currentPlayer+" "+"enter an empty row and column to place your mark!");
    }
    
    public static void printTable() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }
    public static boolean isTableFull() {
        boolean isFull = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    isFull = false;
                }
            }
        }
        return isFull;
    }
    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while(true) {
            row = kb.nextInt();
            col = kb.nextInt();
            if(table[row-1][col-1] == '-') {
                table[row-1][col-1] = currentPlayer;
                return;
            }
        }
    }
    private static boolean isWin() {
        if(checkRow() || checkCol() || checkDiagonals()) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        printNameProgram();
        while(true){
            printText();
            printTable();
            printTurn();
            inputRowCol();
            if(isWin()) {
                printTable();
                printWin();
                break;
            }
            switchPlayer();      
            if(!isWin() && isTableFull()){
                printTable();
                System.out.println("The game was a tie!");
                break;
            }
        }
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Wins!");
    }

    private static boolean checkRow() {
        for(int i = 0; i < 3; i++){
            if(table[row-1][i] != currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkCol() {
        for(int i = 0; i < 3; i++){
            if(table[i][col-1] != currentPlayer) return false;
        }
        return true;
    }
    
    private static boolean checkDiagonals() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer || table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }
}
